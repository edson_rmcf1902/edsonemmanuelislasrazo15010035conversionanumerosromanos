﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Romanos
{
    public partial class Form1 : Form
    {
        Arabico mArabico;
        Romano mRomano;
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            mArabico = new Arabico();
            string romanNumber = TXT_Romano.Text.ToString();
            int arabico = mArabico.SimplerConverter(romanNumber);
            TXT_RomToArab.Text = "" + arabico;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            mRomano = new Romano();
            int arab = int.Parse(TXT_Arabico.Text.ToString());
            string roman = mRomano.ConvertArabicToRoman(arab);
            TXT_ArabToRom.Text = roman;
        }
    }
}
